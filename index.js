require("dotenv").config();

const Telegraf = require("telegraf");
const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");

var screenshot = require("desktop-screenshot");
const session = require("telegraf/session");

const fs = require("fs");
const LocalSession = require("telegraf-session-local");
let bot = new Telegraf(process.env.BOT_TOKEN);
if (process.env.PROXY_USE) {
  console.log("user proxy");
  console.log("proxy host: " + process.env.PROXY_HOST);
  console.log("proxy port : " + process.env.PROXY_PORT);
  const HttpsProxyAgent = require("https-proxy-agent");
  const proxyConfig = {
    telegram: {
      agent: new HttpsProxyAgent({
        host: process.env.PROXY_HOST,
        port: process.env.PROXY_PORT
      })
    }
  };

  // bot.agent = proxyConfig;
  bot = new Telegraf(process.env.BOT_TOKEN, proxyConfig);
  // console.log(bot);
} else {
  const proxyConfig = {};
}

bot.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log("Response time %sms", ms);
  console.log(ctx.message);
});

bot.use(new LocalSession({ database: "example_db.json" }).middleware());

const extra = Extra.markup(
  Markup.inlineKeyboard([Markup.urlButton("❤️", "http://bidv.com.vn")])
);

bot.command("tygia", ctx => {
  console.log("command tygia");

  fscreenshot = __dirname + "/tmp/" + new Date().getTime() + "screenshot.png";

  console.log(fscreenshot);
  extra.caption = "Tỷ giá tại thời điểm: " + new Date().toLocaleString();
  screenshot(fscreenshot, function(error, complete) {
    if (error) console.log("Screenshot failed", error);
    else console.log("Screenshot succeeded");
    ctx.replyWithPhoto({ source: fs.createReadStream(fscreenshot) }, extra);
  });
});

bot.catch(error => {
  console.log(
    "telegraf error",
    error.response,
    error.parameters,
    error.on || error
  );
});

bot.launch();
